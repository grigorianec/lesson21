<?php
/**
 * Created by PhpStorm.
 * User: Максим
 * Date: 16.10.2018
 * Time: 8:56
 */

class ArticlesController extends Controller
{
    public $model = 'articles';

    public function actionIndex()
    {
        $this->view->render('articles/index', [
            'pageTitle' => 'Articles list',
            'articles' => Articles::allArticles(),
        ]);
    }

    public function actionView($id)
    {
        $this->view->render('articles/view', [
            'article' => Articles::findOne($id),
        ]);
    }
}