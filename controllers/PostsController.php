<?php

class PostsController extends Controller
{
    public $model = 'Post';

    public function actionIndex()
    {
        $this->view->render('posts/index', [
            'pageTitle' => 'Posts list',
            'posts' => Post::allPosts(),
        ]);
    }

    public function actionView($id)
    {
        $this->view->render('posts/view', [
            'post' => Post::findOne($id),
        ]);
    }
}