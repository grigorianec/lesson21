<?php
/**
 * Created by PhpStorm.
 * User: Максим
 * Date: 16.10.2018
 * Time: 8:54
 */

class Articles extends Model
{
    public static function allArticles()
    {
        $sql = 'select * from articles';
        $query = static::$db->prepare($sql);
        $query->execute();
        $info = $query->fetchAll();

        return $info;
    }

    public static function findOne($id)
    {
        return self::allArticles()[(int) $id];
    }
}