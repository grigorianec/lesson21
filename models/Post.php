<?php

class Post extends Model
{
    private static $posts = [
        [
            'title' => 'Post 1',
            'short' => 'Some short for post 1',
            'description' => 'Long description for post 1 description for post 1 description for post 1 description for post 1',
        ],
        [
            'title' => 'Post 2',
            'short' => 'Some short for post 2',
            'description' => 'Long description for post 2 description for post 1 description for post 1 description for post 1',
        ],
        [
            'title' => 'Post 3',
            'short' => 'Some short for post 3',
            'description' => 'Long description for post 3 description for post 1 description for post 1 description for post 1',
        ],
    ];

    public static function findAll()
    {
        return self::$posts;
    }

    public static function allPosts()
    {
        $sql = 'select * from posts';
        $query = static::$db->prepare($sql);
        $query->execute();
        $info = $query->fetchAll();

        return $info;
    }

    public static function findOne($id)
    {
        return self::allPosts()[(int) $id];
    }
}