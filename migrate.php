<?php

require_once('components/Model.php');
Model::init();

try {
    $sql = "CREATE TABLE posts (
        id INT NOT NULL auto_increment,
		title VARCHAR(500),
		shortText VARCHAR(500),
		fullDescription VARCHAR(500),
		PRIMARY KEY(id)
	);";
    Model::$db->exec($sql);

    $sql = "CREATE TABLE articles (
        id INT NOT NULL auto_increment,
		title VARCHAR(500),
		shortText VARCHAR(500),
		description VARCHAR(500),
		PRIMARY KEY(id)
	);";
    Model::$db->exec($sql);

    $sql = "INSERT INTO posts (title, shortText, fullDescription)VALUES
        (
        'News','В Киеве, в результате огневого ранения госпитализирован нардеп ...',
        'Депутат находится в реанимации с огнестрельным ранением. Депутат Киеврады Петр Кузик выстрелил себе в живот из
          наградного оружия — пистолета SIG Sauer. Это произошло вечером 13 октября, пишет ИПУ ссылаясь на onlineua.
         Мужчину госпитализировали, он находится в реанимации.'
		  );";
    Model::$db->exec($sql);

    $sql = "INSERT INTO articles (title, shortText, description)VALUES
        (
         'Articles','Tурецкий курорт сотрясло землетрясение ...','Землетрясение
         магнитудой 5,2 произошло в провинции Анталья В Турции в провинции Анталья
          произошло землетрясение магнитудой 5,2'
        );";
    Model::$db->exec($sql);


    echo 'Migrate success';
}catch (Exception $e) {
    echo $e->getMessage();
}
