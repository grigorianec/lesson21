<?php

class Model
{
    public static $db;

    public static function init()
    {
        try {
            self::$db = new PDO(
                'mysql:host=localhost;dbname=framework',
                'member_user',
                '123456'
            );

        } catch (Exception $e) {
            echo 'Cannot create connection';
            die;
        }
    }
}
